<?xml version="1.0"?>
<!-- run with
saxonb-xslt -s:communecter-geo.scrutari-data.xml -xsl:scrutari-to-rdf.xslt

Result:

<rdf:RDF>
  <rdf:Description rdf:about="https://presdecheznous.fr/annuaire#/fiche/acteur/AEr/">
    <rdfs:comment>Vente et visites à la ferme. Châtaignes sèches/farine. Vente (marché de Bastia et foires). Randonnée-visite de la chataîgneraie, Techniques de ramassage/transformation des chataîgnes. Repas champêtre. Minimum 7 personnes (réservation obligatoire).</rdfs:comment>
    <rdfs:label>A Nebbiulinca</rdfs:label>
    <geo:lat rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">42.57679</geo:lat>
    <geo:long rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">9.3266</geo:long>
    <foaf:homepage rdf:resource="http://www.anebbiulinca.com"/>
  </rdf:Description>
</rdf:RDF>
-->

<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform'

 xmlns:foaf="http://xmlns.com/foaf/0.1/"
   xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
   xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:dcat="http://www.w3.org/ns/dcat#"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   version="2.0" >

 <xsl:output method="xml" indent="yes" />

 <!-- -->
 <xsl:template match="base">
  <rdf:RDF>
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
  </rdf:RDF>
</xsl:template>

 <xsl:template match="corpus">
    <xsl:apply-templates select="../base-metadata" mode="inside-rdf"/>
    <xsl:apply-templates select="*|text()|processing-instruction()|comment()" />
 </xsl:template>
 <xsl:template match="corpus-metadata | base-metadata"></xsl:template>
 <xsl:template match="base-name"></xsl:template>
 <xsl:template match="langs-ui"></xsl:template>
 <xsl:template match="corpus-name"></xsl:template>
 <!-- TODO -->
 <xsl:template match="thesaurus"></xsl:template>
 <xsl:template match="indexation-group"></xsl:template>

 <xsl:template match="base-metadata" mode="inside-rdf">
  <!-- http://sct1.scrutari.net/data/presdecheznous/presdecheznous-geo.scrutari-data.xml -->
  <dcat:Dataset rdf:about="http://sct1.scrutari.net/data/">
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
  </dcat:Dataset>
 </xsl:template>

 <xsl:template match="base-icon">
  <foaf:img>
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
  </foaf:img>
 </xsl:template>



 <xsl:template match="fiche">
  <foaf:Organization xml:lang="{lang}">
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
    <dcterms:publisher rdf:resource="http://{ //authority/text() }" />
  </foaf:Organization>
 </xsl:template>

 <xsl:template match="@fiche-id">
    <xsl:attribute name="rdf:about" >
      <xsl:value-of select='../href/text()' />
    </xsl:attribute>
    <!-- Rappers says: Using an attribute 'fiche-id' without a namespace is forbidden.
         <xsl:copy-of select='.' /> -->
    <xsl:attribute name="rdf:fiche-id" select="."/>
 </xsl:template>
 <xsl:template match="href"></xsl:template>
 <xsl:template match="lang"></xsl:template>

 <xsl:template match="titre | intitule-short | authority">
   <rdfs:label>
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
   </rdfs:label>
 </xsl:template>

 <xsl:template match="soustitre">
   <rdfs:comment>
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
   </rdfs:comment>
 </xsl:template>

 <xsl:template match="intitule-long">
   <dc:description>
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
   </dc:description>
 </xsl:template>

 <xsl:template match="geoloc / lat">
  <geo:lat rdf:datatype="http://www.w3.org/2001/XMLSchema#float">
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
  </geo:lat>
 </xsl:template>
 <xsl:template match="geoloc / lon">
  <geo:long rdf:datatype="http://www.w3.org/2001/XMLSchema#float">
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
  </geo:long>
 </xsl:template>

 <xsl:template match='attr [@key="website"] '>
    <!--NOTE: castable as xs:anyURI catches almost no URI error !
     xsl:message>   castable <xsl:value-of select='val / text() castable as xs:anyURI' /> </xsl:message -->
    <xsl:apply-templates select="val" />
 </xsl:template>

 <xsl:template match='attr [@key="city"]'>
  <foaf:based_near>
    <xsl:apply-templates select="*" />
  </foaf:based_near>
 </xsl:template>

 <xsl:template match='attr [@key="tags"]'>
   <dcterms:subject>
    <xsl:apply-templates select="*" />
   </dcterms:subject>
 </xsl:template>

 <!-- From example by Conal T. -->
 <xsl:template match='val [../@key="website"]'>
    <xsl:message>    URL analyze-string !!! <xsl:value-of select='.' /> </xsl:message>
  <foaf:homepage>
   <xsl:attribute name="rdf:resource">
    <xsl:analyze-string select="." regex="(https?://[^\?/]*)([^?#]*)(.*)">
     <xsl:matching-substring>
<!-- regex-group(1) = scheme and host -->
<!-- regex-group(2) = path -->
<!-- regex-group(3) = query and fragment id -->

     <xsl:variable name="fixedSchemeAndHost" select="
      replace(
       replace(
        replace( regex-group(1), ' ', ''),
         '@gmail.com', '.com' ),
          '.laposte.net', '.net'
      )    " />
     <xsl:if test='not( xs:string(current()) = $fixedSchemeAndHost )' >
      <xsl:message> URL corrigé: <xsl:value-of select='regex-group(1)'/> ==) <xsl:value-of select='$fixedSchemeAndHost'/> </xsl:message>
     </xsl:if>

      <xsl:value-of select="$fixedSchemeAndHost"/>
      <xsl:message>      URL scheme and host (original) <xsl:value-of select='regex-group(1)' /> </xsl:message>
      <xsl:analyze-string select="regex-group(2)" regex="[/a-zA-Z0-9\-\._~]">
       <!-- matches any character OK in a URI path -->
        <xsl:matching-substring>
         <xsl:value-of select="."/>
         <xsl:message>      URL path <xsl:value-of select='regex-group(1)' /> </xsl:message>
        </xsl:matching-substring>
        <!-- characters that aren't OK get encoded -->
        <xsl:non-matching-substring>
         <xsl:value-of select="encode-for-uri(.)"/>
         <xsl:message>      URL path encode-for-uri <xsl:value-of select='encode-for-uri(.)' /> </xsl:message>
        </xsl:non-matching-substring>
      </xsl:analyze-string>
      <xsl:value-of select="regex-group(3)"/>
      <xsl:message>      URL query and fragment id <xsl:value-of select='regex-group(3)' /> </xsl:message>
     </xsl:matching-substring>
    </xsl:analyze-string>
   </xsl:attribute>
  </foaf:homepage>
 </xsl:template>

</xsl:stylesheet>
