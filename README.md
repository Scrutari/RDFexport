# Les données XML

Les données XML au format Scrutari sont ici:
http://sct1.scrutari.net/sct/geosse/export/source-list.html

# Exécuter

Voir le commentaire dans le XSLT

# A FAIRE
TODO

- traiter `<corpus corpus-name="project">` dans communecter-geo.scrutari-data.xml, afin de créer des foaf:Project
- traiter `<thesaurus thesaurus-name="categorie">` dans presdecheznous-geo.scrutari-data.xml, en SKOS
- ajouter un triplet dans chaque fiche Organisation, etc, pour indiquer la provenance:  communecter, presdecheznous
- villes: mettre des URI dbpedia:

